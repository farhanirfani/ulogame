ULO
===

## Developer Info
4210181015 - [Muhammad Farhan Irfani](https://gitlab.com/farhanirfani) <br>
4210181027 - [Muhammad Amien Prananto](https://gitlab.com/minamin420) <br>
Course : Desain Game Multiplayer Online

## Game Overview
**Description** <br>
In this game, the player will become a dragon character where this character only has 2 goals, namely eating food scattered throughout the map and avoiding collisions by other players. The concept of this game is almost similar to Tron, where to win the game you must destroy the enemy by blocking the path. But in this game, to be able to block the opponent's path, you have to eat the food scattered on the map to lengthen his body.

**Feature** <br>
1. Login System <br>
2. Multiplayer <br>

**Data transferred using TCP** <br>
1. Login Data <br>
2. Login Result <br>
3. Spawn Player <br>
4. Spawn food <br>

**Data transferred using UDP** <br>
1. Player Movement  <br>

**Game Information** <br>
*Platform*   :  Desktop (Windows) <br>
*Rating*   : Everyone 10+ <br>
*Network Protocol* : TCP and UDP <br>

**Project Information** <br>
*Game Engine* : Unity 2019 LTS <br>

## Game Login Menu
![](https://gitlab.com/farhanirfani/ulogame/-/raw/main/Screenshot/menu.png)

Codes
---
## Client Side
**Client.cs** <br>
Set up connection from client to server & initialize packet <br>
```
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System;

public class Client : MonoBehaviour
{
    public static Client instance;

    public static int dataBufferSize = 4096;

    public string ip;
    public int port;
    
    public int myId = 0;

    public string username;
    
    public TCP tcp;
    public UDP udp;

    private delegate void PacketHandler(Packet _packet);
    private static Dictionary<int, PacketHandler> packetHandlers;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this);
        }
    }

    private void OnApplicationQuit()
    {
        Disconnect();
    }

    public void ConnectToServer()
    {
        tcp = new TCP();
        udp = new UDP();

        InitializeClientData();

        tcp.Connect();
    }

    public class TCP
    {
        public TcpClient socket;

        private NetworkStream stream;
        private Packet receivedData;
        private byte[] receiveBuffer;

        public void Connect()
        {
            socket = new TcpClient
            {
                ReceiveBufferSize = dataBufferSize,
                SendBufferSize = dataBufferSize
            };

            receiveBuffer = new byte[dataBufferSize];
            socket.BeginConnect(instance.ip, instance.port, ConnectCallback, socket);
        }

        private void ConnectCallback(IAsyncResult _result)
        {
            socket.EndConnect(_result);

            if (!socket.Connected)
            {
                return;
            }

            stream = socket.GetStream();

            receivedData = new Packet();

            stream.BeginRead(receiveBuffer, 0, dataBufferSize, ReceiveCallback, null);
        }

        public void SendData(Packet _packet)
        {
            try
            {
                if (socket != null)
                {
                    stream.BeginWrite(_packet.ToArray(), 0, _packet.Length(), null, null);
                }
            }
            catch (Exception _ex)
            {
                Debug.Log($"Error sending data to server via TCP: {_ex}");
            }
        }

        private void ReceiveCallback(IAsyncResult _result)
        {
            try
            {
                int _byteLength = stream.EndRead(_result);
                if (_byteLength <= 0)
                {
                    instance.Disconnect();
                    return;
                }

                byte[] _data = new byte[_byteLength];
                Array.Copy(receiveBuffer, _data, _byteLength);

                receivedData.Reset(HandleData(_data));
                stream.BeginRead(receiveBuffer, 0, dataBufferSize, ReceiveCallback, null);
            }
            catch
            {
                Disconnect();
            }
        }

        private bool HandleData(byte[] _data)
        {
            int _packetLength = 0;

            receivedData.SetBytes(_data);

            if (receivedData.UnreadLength() >= 4)
            {
                _packetLength = receivedData.ReadInt();
                if (_packetLength <= 0)
                {
                    return true;
                }
            }

            while (_packetLength > 0 && _packetLength <= receivedData.UnreadLength())
            {
                byte[] _packetBytes = receivedData.ReadBytes(_packetLength);
                ThreadManager.ExecuteOnMainThread(() =>
                {
                    using (Packet _packet = new Packet(_packetBytes))
                    {
                        int _packetId = _packet.ReadInt();
                        packetHandlers[_packetId](_packet);
                    }
                });

                _packetLength = 0;
                if (receivedData.UnreadLength() >= 4)
                {
                    _packetLength = receivedData.ReadInt();
                    if (_packetLength <= 0)
                    {
                        return true;
                    }
                }
            }

            if (_packetLength <= 1)
            {
                return true;
            }

            return false;
        }

        private void Disconnect()
        {
            instance.Disconnect();

            stream = null;
            receivedData = null;
            receiveBuffer = null;
            socket = null;
        }
    }

    public class UDP
    {
        public UdpClient socket;
        public IPEndPoint endPoint;

        public UDP()
        {
            endPoint = new IPEndPoint(IPAddress.Parse(instance.ip), instance.port);
        }

        public void Connect(int _localPort)
        {
            socket = new UdpClient(_localPort);

            socket.Connect(endPoint);
            socket.BeginReceive(ReceiveCallback, null);

            using (Packet _packet = new Packet())
            {
                SendData(_packet);
            }
        }

        public void SendData(Packet _packet)
        {
            try
            {
                _packet.InsertInt(instance.myId);
                if (socket != null)
                {
                    socket.BeginSend(_packet.ToArray(), _packet.Length(), null, null);
                }
            }
            catch (Exception _ex)
            {
                Debug.Log($"Error sending data to server via UDP: {_ex}");
            }
        }

        private void ReceiveCallback(IAsyncResult _result)
        {
            try
            {
                byte[] _data = socket.EndReceive(_result, ref endPoint);
                socket.BeginReceive(ReceiveCallback, null);

                if (_data.Length < 4)
                {
                    instance.Disconnect();
                    return;
                }

                HandleData(_data);
            }
            catch
            {
                Disconnect();
            }
        }

        private void HandleData(byte[] _data)
        {
            using (Packet _packet = new Packet(_data))
            {
                int _packetLength = _packet.ReadInt();
                _data = _packet.ReadBytes(_packetLength);
            }

            ThreadManager.ExecuteOnMainThread(() =>
            {
                using (Packet _packet = new Packet(_data))
                {
                    int _packetId = _packet.ReadInt();
                    packetHandlers[_packetId](_packet);
                }
            });
        }

        private void Disconnect()
        {
            instance.Disconnect();

            endPoint = null;
            socket = null;
        }
    }

    private void InitializeClientData()
    {
        packetHandlers = new Dictionary<int, PacketHandler>()
        {
            { (int)ServerPackets.tcpTest, ClientHandle.TcpTest },
            { (int)ServerPackets.udpTest, ClientHandle.UdpTest },
            { (int)ServerPackets.login, ClientHandle.Login },
            { (int)ServerPackets.signUp, ClientHandle.SignUp },
            { (int)ServerPackets.playerDisconnected, ClientHandle.PlayerDisconnected },
            { (int)ServerPackets.playerSpawn, ClientHandle.playerSpawn },
            { (int)ServerPackets.playerPosition, ClientHandle.PlayerPosition },
            { (int)ServerPackets.playerRotation, ClientHandle.PlayerRotation },
            { (int)ServerPackets.spawnFood, ClientHandle.SpawnFood },
            { (int)ServerPackets.destroyFood, ClientHandle.DestroyFood },
        };
    }

    public void Disconnect()
    {
        tcp.socket.Close();
        udp.socket.Close();

        Debug.Log("Disconnected from server.");
    }
}
```


**ClientHandle.cs** <br>
Contains the functions that the client will perform according to the received package <br>
using System;
using System.Net;
using UnityEngine;

```
public class ClientHandle : MonoBehaviour
{
    public static void TcpTest(Packet _packet)
    {
        int _myId = _packet.ReadInt(); 
        Client.instance.myId = _myId;
        Debug.Log("TCP Connected");

        ClientSend.TCPTestReceived();
        Client.instance.udp.Connect(((IPEndPoint)Client.instance.tcp.socket.Client.LocalEndPoint).Port);
    }

    public static void UdpTest(Packet _packet)
    {
        Debug.Log("UDP Connected");
    }

    public static void Login(Packet _packet)
    {
        bool _success = _packet.ReadBool();
        string _message = _packet.ReadString();

        if(_success)
        {
            string _username = _packet.ReadString();
            Client.instance.username = _username;
            AuthManager.instance.playButton.interactable = true;
            //LoadScene("Game");
        }
        
        AuthManager.instance.statusText.text = _message;
    }

    public static void SignUp(Packet _packet)
    {
        bool success = _packet.ReadBool();
        string _message = _packet.ReadString();

        if (success)
        {
            AuthManager.instance.ResetInput();
        }
        AuthManager.instance.statusText.text = _message;
    }

    public static void playerSpawn(Packet _packet)
    {
        int _id = _packet.ReadInt();
        string _username = _packet.ReadString();
        Vector3 position = _packet.ReadVector3();
        Quaternion rotation = _packet.ReadQuaternion();
        Debug.Log(position);
        GameManager.instance.SpawnPlayer(_id, _username, position, rotation);
    }

    public static void PlayerPosition(Packet _packet)
    {
        int _id = _packet.ReadInt();
        if (GameManager.instance.players.ContainsKey(_id))
        {
            Vector3 _position = _packet.ReadVector3();
            //Debug.Log(_position);
            GameManager.instance.players[_id].transform.position = _position;
        }

    }

    public static void PlayerRotation(Packet _packet)
    {
        int _id = _packet.ReadInt();
        if (GameManager.instance.players.ContainsKey(_id))
        {
            Quaternion _rotation = _packet.ReadQuaternion();

            GameManager.instance.players[_id].transform.rotation = _rotation;
        }
    }

    public static void PlayerDisconnected(Packet _packet)
    {
        string _id = _packet.ReadString();

        Debug.Log("player " + _id + " is disconnected");
    }

    public static void SpawnFood(Packet _packet)
    {
        int _foodId = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();

        GameManager.instance.SpawnFood(_foodId, _position);
    }

    public static void DestroyFood(Packet _packet)
    {
        int _foodId = _packet.ReadInt();
        GameManager.instance.FoodDestroy(_foodId);
    }
}
```


**ClientSend.cs** <br>
to send the package to server<br>
```
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientSend : MonoBehaviour
{
    private static void SendTCPData(Packet _packet)
    {
        _packet.WriteLength();
        Client.instance.tcp.SendData(_packet);
    }

    private static void SendUDPData(Packet _packet)
    {
        _packet.WriteLength();
        Client.instance.udp.SendData(_packet);
    }

    #region Packets
    public static void TCPTestReceived()
    {
        using (Packet _packet = new Packet((int)ClientPackets.tcpTestReceived))
        {
            _packet.Write(Client.instance.myId);
            
            SendTCPData(_packet);
        }
    }

    public static void LoginInput(string _username, string _password)
    {
        using (Packet _packet = new Packet((int)ClientPackets.loginInput))
        {
            _packet.Write(_username);
            _packet.Write(_password);

            SendTCPData(_packet);
        }
    }

    public static void SignUpInput(string _username, string _password)
    {
        using (Packet _packet = new Packet((int)ClientPackets.signUpInput))
        {
            _packet.Write(_username);
            _packet.Write(_password);

            SendTCPData(_packet);
        }
    }

    public static void PlayerMovement(bool[] _inputs)
    {
        using(Packet _packet = new Packet((int)ClientPackets.playerMovement))
        {
            _packet.Write(_inputs.Length);
            foreach (bool _input in _inputs)
            {
                _packet.Write(_input);
            }
            _packet.Write(GameManager.instance.players[Client.instance.myId].transform.rotation);
            SendUDPData(_packet);
        }
    }
    #endregion
}
```

**Packet.cs** <br>
Initialize the enumerator used in the package to be sent.<br>
```
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

/// <summary>Sent from server to client.</summary>
public enum ServerPackets
{
    tcpTest = 1,
    udpTest,
    login,
    signUp,
    playerConnect,
    playerDisconnected,
    spawnFood,
    destroyFood,
    playerSpawn,
    playerPosition,
    playerRotation
}

/// <summary>Sent from client to server.</summary>
public enum ClientPackets
{
    tcpTestReceived = 1,
    loginInput,
    signUpInput,
    playerMovement,
    playerEat
}

public class Packet : IDisposable
{
    private List<byte> buffer;
    private byte[] readableBuffer;
    private int readPos;

    /// <summary>Creates a new empty packet (without an ID).</summary>
    public Packet()
    {
        buffer = new List<byte>(); // Intitialize buffer
        readPos = 0; // Set readPos to 0
    }

    /// <summary>Creates a new packet with a given ID. Used for sending.</summary>
    /// <param name="_id">The packet ID.</param>
    public Packet(int _id)
    {
        buffer = new List<byte>(); // Intitialize buffer
        readPos = 0; // Set readPos to 0

        Write(_id); // Write packet id to the buffer
    }

    /// <summary>Creates a packet from which data can be read. Used for receiving.</summary>
    /// <param name="_data">The bytes to add to the packet.</param>
    public Packet(byte[] _data)
    {
        buffer = new List<byte>(); // Intitialize buffer
        readPos = 0; // Set readPos to 0

        SetBytes(_data);
    }

    #region Functions
    /// <summary>Sets the packet's content and prepares it to be read.</summary>
    /// <param name="_data">The bytes to add to the packet.</param>
    public void SetBytes(byte[] _data)
    {
        Write(_data);
        readableBuffer = buffer.ToArray();
    }

    /// <summary>Inserts the length of the packet's content at the start of the buffer.</summary>
    public void WriteLength()
    {
        buffer.InsertRange(0, BitConverter.GetBytes(buffer.Count)); // Insert the byte length of the packet at the very beginning
    }

    /// <summary>Inserts the given int at the start of the buffer.</summary>
    /// <param name="_value">The int to insert.</param>
    public void InsertInt(int _value)
    {
        buffer.InsertRange(0, BitConverter.GetBytes(_value)); // Insert the int at the start of the buffer
    }

    /// <summary>Gets the packet's content in array form.</summary>
    public byte[] ToArray()
    {
        readableBuffer = buffer.ToArray();
        return readableBuffer;
    }

    /// <summary>Gets the length of the packet's content.</summary>
    public int Length()
    {
        return buffer.Count; // Return the length of buffer
    }

    /// <summary>Gets the length of the unread data contained in the packet.</summary>
    public int UnreadLength()
    {
        return Length() - readPos; // Return the remaining length (unread)
    }

    /// <summary>Resets the packet instance to allow it to be reused.</summary>
    /// <param name="_shouldReset">Whether or not to reset the packet.</param>
    public void Reset(bool _shouldReset = true)
    {
        if (_shouldReset)
        {
            buffer.Clear(); // Clear buffer
            readableBuffer = null;
            readPos = 0; // Reset readPos
        }
        else
        {
            readPos -= 4; // "Unread" the last read int
        }
    }
    #endregion

    #region Write Data
    /// <summary>Adds a byte to the packet.</summary>
    /// <param name="_value">The byte to add.</param>
    public void Write(byte _value)
    {
        buffer.Add(_value);
    }
    /// <summary>Adds an array of bytes to the packet.</summary>
    /// <param name="_value">The byte array to add.</param>
    public void Write(byte[] _value)
    {
        buffer.AddRange(_value);
    }
    /// <summary>Adds a short to the packet.</summary>
    /// <param name="_value">The short to add.</param>
    public void Write(short _value)
    {
        buffer.AddRange(BitConverter.GetBytes(_value));
    }
    /// <summary>Adds an int to the packet.</summary>
    /// <param name="_value">The int to add.</param>
    public void Write(int _value)
    {
        buffer.AddRange(BitConverter.GetBytes(_value));
    }
    /// <summary>Adds a long to the packet.</summary>
    /// <param name="_value">The long to add.</param>
    public void Write(long _value)
    {
        buffer.AddRange(BitConverter.GetBytes(_value));
    }
    /// <summary>Adds a float to the packet.</summary>
    /// <param name="_value">The float to add.</param>
    public void Write(float _value)
    {
        buffer.AddRange(BitConverter.GetBytes(_value));
    }
    /// <summary>Adds a bool to the packet.</summary>
    /// <param name="_value">The bool to add.</param>
    public void Write(bool _value)
    {
        buffer.AddRange(BitConverter.GetBytes(_value));
    }
    /// <summary>Adds a string to the packet.</summary>
    /// <param name="_value">The string to add.</param>
    public void Write(string _value)
    {
        Write(_value.Length); // Add the length of the string to the packet
        buffer.AddRange(Encoding.ASCII.GetBytes(_value)); // Add the string itself
    }
    /// <summary>Adds a Vector3 to the packet.</summary>
    /// <param name="_value">The Vector3 to add.</param>
    public void Write(Vector3 _value)
    {
        Write(_value.x);
        Write(_value.y);
        Write(_value.z);
    }
    /// <summary>Adds a Quaternion to the packet.</summary>
    /// <param name="_value">The Quaternion to add.</param>
    public void Write(Quaternion _value)
    {
        Write(_value.x);
        Write(_value.y);
        Write(_value.z);
        Write(_value.w);
    }
    #endregion

    #region Read Data
    /// <summary>Reads a byte from the packet.</summary>
    /// <param name="_moveReadPos">Whether or not to move the buffer's read position.</param>
    public byte ReadByte(bool _moveReadPos = true)
    {
        if (buffer.Count > readPos)
        {
            // If there are unread bytes
            byte _value = readableBuffer[readPos]; // Get the byte at readPos' position
            if (_moveReadPos)
            {
                // If _moveReadPos is true
                readPos += 1; // Increase readPos by 1
            }
            return _value; // Return the byte
        }
        else
        {
            throw new Exception("Could not read value of type 'byte'!");
        }
    }

    /// <summary>Reads an array of bytes from the packet.</summary>
    /// <param name="_length">The length of the byte array.</param>
    /// <param name="_moveReadPos">Whether or not to move the buffer's read position.</param>
    public byte[] ReadBytes(int _length, bool _moveReadPos = true)
    {
        if (buffer.Count > readPos)
        {
            // If there are unread bytes
            byte[] _value = buffer.GetRange(readPos, _length).ToArray(); // Get the bytes at readPos' position with a range of _length
            if (_moveReadPos)
            {
                // If _moveReadPos is true
                readPos += _length; // Increase readPos by _length
            }
            return _value; // Return the bytes
        }
        else
        {
            throw new Exception("Could not read value of type 'byte[]'!");
        }
    }

    /// <summary>Reads a short from the packet.</summary>
    /// <param name="_moveReadPos">Whether or not to move the buffer's read position.</param>
    public short ReadShort(bool _moveReadPos = true)
    {
        if (buffer.Count > readPos)
        {
            // If there are unread bytes
            short _value = BitConverter.ToInt16(readableBuffer, readPos); // Convert the bytes to a short
            if (_moveReadPos)
            {
                // If _moveReadPos is true and there are unread bytes
                readPos += 2; // Increase readPos by 2
            }
            return _value; // Return the short
        }
        else
        {
            throw new Exception("Could not read value of type 'short'!");
        }
    }

    /// <summary>Reads an int from the packet.</summary>
    /// <param name="_moveReadPos">Whether or not to move the buffer's read position.</param>
    public int ReadInt(bool _moveReadPos = true)
    {
        if (buffer.Count > readPos)
        {
            // If there are unread bytes
            int _value = BitConverter.ToInt32(readableBuffer, readPos); // Convert the bytes to an int
            if (_moveReadPos)
            {
                // If _moveReadPos is true
                readPos += 4; // Increase readPos by 4
            }
            return _value; // Return the int
        }
        else
        {
            throw new Exception("Could not read value of type 'int'!");
        }
    }

    /// <summary>Reads a long from the packet.</summary>
    /// <param name="_moveReadPos">Whether or not to move the buffer's read position.</param>
    public long ReadLong(bool _moveReadPos = true)
    {
        if (buffer.Count > readPos)
        {
            // If there are unread bytes
            long _value = BitConverter.ToInt64(readableBuffer, readPos); // Convert the bytes to a long
            if (_moveReadPos)
            {
                // If _moveReadPos is true
                readPos += 8; // Increase readPos by 8
            }
            return _value; // Return the long
        }
        else
        {
            throw new Exception("Could not read value of type 'long'!");
        }
    }

    /// <summary>Reads a float from the packet.</summary>
    /// <param name="_moveReadPos">Whether or not to move the buffer's read position.</param>
    public float ReadFloat(bool _moveReadPos = true)
    {
        if (buffer.Count > readPos)
        {
            // If there are unread bytes
            float _value = BitConverter.ToSingle(readableBuffer, readPos); // Convert the bytes to a float
            if (_moveReadPos)
            {
                // If _moveReadPos is true
                readPos += 4; // Increase readPos by 4
            }
            return _value; // Return the float
        }
        else
        {
            throw new Exception("Could not read value of type 'float'!");
        }
    }

    /// <summary>Reads a bool from the packet.</summary>
    /// <param name="_moveReadPos">Whether or not to move the buffer's read position.</param>
    public bool ReadBool(bool _moveReadPos = true)
    {
        if (buffer.Count > readPos)
        {
            // If there are unread bytes
            bool _value = BitConverter.ToBoolean(readableBuffer, readPos); // Convert the bytes to a bool
            if (_moveReadPos)
            {
                // If _moveReadPos is true
                readPos += 1; // Increase readPos by 1
            }
            return _value; // Return the bool
        }
        else
        {
            throw new Exception("Could not read value of type 'bool'!");
        }
    }

    /// <summary>Reads a string from the packet.</summary>
    /// <param name="_moveReadPos">Whether or not to move the buffer's read position.</param>
    public string ReadString(bool _moveReadPos = true)
    {
        try
        {
            int _length = ReadInt(); // Get the length of the string
            string _value = Encoding.ASCII.GetString(readableBuffer, readPos, _length); // Convert the bytes to a string
            if (_moveReadPos && _value.Length > 0)
            {
                // If _moveReadPos is true string is not empty
                readPos += _length; // Increase readPos by the length of the string
            }
            return _value; // Return the string
        }
        catch
        {
            throw new Exception("Could not read value of type 'string'!");
        }
    }

    /// <summary>Reads a Vector3 from the packet.</summary>
    /// <param name="_moveReadPos">Whether or not to move the buffer's read position.</param>
    public Vector3 ReadVector3(bool _moveReadPos = true)
    {
        return new Vector3(ReadFloat(_moveReadPos), ReadFloat(_moveReadPos), ReadFloat(_moveReadPos));
    }

    /// <summary>Reads a Quaternion from the packet.</summary>
    /// <param name="_moveReadPos">Whether or not to move the buffer's read position.</param>
    public Quaternion ReadQuaternion(bool _moveReadPos = true)
    {
        return new Quaternion(ReadFloat(_moveReadPos), ReadFloat(_moveReadPos), ReadFloat(_moveReadPos), ReadFloat(_moveReadPos));
    }
    #endregion

    private bool disposed = false;

    protected virtual void Dispose(bool _disposing)
    {
        if (!disposed)
        {
            if (_disposing)
            {
                buffer = null;
                readableBuffer = null;
                readPos = 0;
            }

            disposed = true;
        }
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
}
```


**AuthManager.cs** <br>
sending login/signup input to server and show the server respon <br>
```
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
public class AuthManager : MonoBehaviour
{
    public static AuthManager instance;

    public TMP_InputField usernameInput;
    public TMP_InputField passwordInput;
    public TextMeshProUGUI statusText;
    public Button playButton;
    public GameObject panelLogin;
    public GameObject creditPanel;
    public GameObject scoreText;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this);
        }
    }

    void Start()
    {
        statusText.text = "";
    }

    public void ResetInput()
    {
        usernameInput.text = "";
        passwordInput.text = "";
    }
    public void Login()
    {
        ClientSend.LoginInput(usernameInput.text, passwordInput.text);
    }

    public void SignUp()
    {
        ClientSend.SignUpInput(usernameInput.text, passwordInput.text);
    }

    public void Play()
    {
        panelLogin.transform.localScale = new Vector3 (0, 0, 0);
        scoreText.transform.localScale = new Vector3 (1, 1, 1);
    }

    public void Credit()
    {
        creditPanel.transform.localScale = new Vector3(1, 1, 1);
    }

    public void BackFromCredit()
    {
        creditPanel.transform.localScale = new Vector3(0, 0, 0);
    }
}
```

**food.cs** <br>
initialize food <br>
```
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour
{
    public int foodId;

    // Start is called before the first frame update
    public void Initialize(int _foodId)
    {
        foodId = _foodId;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Score.score++;
            Debug.Log(Score.score);
            FoodDestroy(foodId);
        }
    }
    public void FoodDestroy(int _foodId)
    {
        Destroy(GameManager.foods[_foodId].gameObject);
        GameManager.foods.Remove(_foodId);
    }
}
```


**GameManager.cs** <br>
spawn player <br>
```
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public Dictionary<int, Player> players = new Dictionary<int, Player>();
    public static Dictionary<int, Food> foods = new Dictionary<int, Food>();

    public Food foodPrefab;

    public GameObject localPrefab, playerPrefab;

    // Start is called before the first frame update
    void Start()
    {
        Client.instance.ConnectToServer();
    }

    public void SpawnPlayer(int _id, string _username, Vector3 position, Quaternion rotation)
    {
        GameObject _player;
        if(_id == Client.instance.myId)
        {
            _player = Instantiate(localPrefab, position, rotation);
        }
        else
        {
            _player = Instantiate(playerPrefab, position, rotation);
        }

        _player.transform.SetParent(this.transform, false);
        _player.GetComponent<Player>().Initialize(_id, _username);
        //_player.GetComponent<Player>().id = _id;
        //_player.GetComponent<Player>().username = _username;
        players.Add(_id, _player.GetComponent<Player>());
    }
    
    public void SpawnFood(int _foodId, Vector3 _position)
    {
        Food food = Instantiate(foodPrefab, _position, foodPrefab.transform.rotation) as Food;
        food.Initialize(_foodId);
        foods.Add(_foodId, food);
    }

    public void FoodDestroy(int _foodId)
    {
            Destroy(foods[_foodId].gameObject);
            foods.Remove(_foodId);
    }
}
```


**GameOver.cs** <br>
game over scene if the game ended <br>
```
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    public static bool isWin;
    public GameObject winText, loseText;
    // Start is called before the first frame update
    void Start()
    {
        if (isWin)
        {
            winText.SetActive(true);
        } else
        {
            loseText.SetActive(false);  
        }
    }

    public void BackButton()
    {
        SceneManager.LoadScene("Menu");
    }

    public void ExitButton()
    {
        Application.Quit();
    }

}
```


**Player.cs** <br>
initialize id and username player <br>
```
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Pipes;
using System.Text;
using System;

public class Player : MonoBehaviour
{
    /*public int id;
    public string username;

    private void LateUpdate()
    {        
        //PlayerMovement();
    }

    public void Initialize(int _id)
    {
        id = _id;
        username = Server.clients[_id].username;
    }

    public void PlayerMovement()
    {
        //ServerSend.SendPlayerPosition(this);
        //ServerSend.SendPlayerRotation(this);
    }*/

    public int id;
    public int food;
    public string username;

    public void Initialize(int _id, string _username)
    {
        id = _id;
        username = _username;
    }

    private void Start()
    {
       
    }

  
    public void AddFood()
    {
        food++;
        //ServerSend.PlayerCoin(id, food);
    }

}
```


**PlayerController.cs** <br>
sending keyboard input to server and detect the collison <br>
```
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
   
    public float waitTime = 2;
    private void FixedUpdate()
    {
        SendnputToServer();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            GameOver.isWin = false;
            Client.instance.Disconnect();
            SceneManager.LoadScene("GameOver");
        }
    }
    private void SendnputToServer()
    {
        bool[] _inputs = new bool[]
        {
            Input.GetKey(KeyCode.W),
            Input.GetKey(KeyCode.A),
            Input.GetKey(KeyCode.S),
            Input.GetKey(KeyCode.D),
        };

        ClientSend.PlayerMovement(_inputs);
    }
}
```

**Score.cs** <br>
player's score <br>
```
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Score : MonoBehaviour
{
    public static Score instance;
    public Text scoreText;
    public static int score;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "Score : " + score;
        
        if(score == 10)
        {
            GameOver.isWin = true;
            Client.instance.Disconnect();
            SceneManager.LoadScene("GameOver");
        }
    }
}
```


**SpawnFood.cs** <br>
spawning food <br>
```
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFood : MonoBehaviour
{
    public static Dictionary<int, Food> food = new Dictionary<int, Food>();
    private int idCount;

    public Vector3 spawnValues;

    public float spawnMostWait;
    public float spawnLeastWait;
    private float spawnWait;

    public bool startSpawn;

    [Header("Prefab")]
    public Food foodPrefab;

    private void Start()
    {
        spawnWait = Random.Range(spawnLeastWait, spawnMostWait);
    }

    private void Update()
    {
        if (!startSpawn) return;

        if (spawnWait > 0)
        {
            spawnWait -= Time.deltaTime;
        }
        else
        {
            Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), Random.Range(-spawnValues.y, spawnValues.y), 0);
            spawnPosition += this.transform.position;

            //ServerSend.spawnFood(idCount, spawnPosition);
            Food food = Instantiate(foodPrefab, spawnPosition, foodPrefab.transform.rotation) as Food;

            food.Initialize(idCount);
            idCount++;

            //food.Add(idCount, food);

            spawnWait = Random.Range(spawnLeastWait, spawnMostWait);
        }

    }

}
```


## Server Side
**Client.cs** <br>
set up the socket for accept player's data <br>
```
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

public class Client
{
    public static int dataBufferSize = 4096;

    public int id;
    public string username;

    public User user;
    public Player player;

    public TCP tcp;
    public UDP udp;

    public Client(int _clientId)
    {
        id = _clientId;
        tcp = new TCP(id);
        udp = new UDP(id);
    }

    public class TCP
    {
        public TcpClient socket;

        private readonly int id;
        private NetworkStream stream;
        private Packet receivedData;
        private byte[] receiveBuffer;

        public TCP(int _id)
        {
            id = _id;
        }

        public void Connect(TcpClient _socket)
        {
            socket = _socket;
            socket.ReceiveBufferSize = dataBufferSize;
            socket.SendBufferSize = dataBufferSize;

            stream = socket.GetStream();

            receivedData = new Packet();
            receiveBuffer = new byte[dataBufferSize];

            stream.BeginRead(receiveBuffer, 0, dataBufferSize, ReceiveCallback, null);

            ServerSend.TCPTest(id);
        }

        public void SendData(Packet _packet)
        {
            try
            {
                if (socket != null)
                {
                    stream.BeginWrite(_packet.ToArray(), 0, _packet.Length(), null, null);
                }
            }
            catch (Exception _ex)
            {
                Debug.Log($"Error sending data to player {id} via TCP: {_ex}");
            }
        }

        private void ReceiveCallback(IAsyncResult _result)
        {
            try
            {
                int _byteLength = stream.EndRead(_result);
                if (_byteLength <= 0)
                {
                    Server.clients[id].Disconnect();
                    return;
                }

                byte[] _data = new byte[_byteLength];
                Array.Copy(receiveBuffer, _data, _byteLength);

                receivedData.Reset(HandleData(_data));
                stream.BeginRead(receiveBuffer, 0, dataBufferSize, ReceiveCallback, null);
            }
            catch (Exception _ex)
            {
                Debug.Log($"Error receiving TCP data: {_ex}");
                Server.clients[id].Disconnect();
            }
        }

        private bool HandleData(byte[] _data)
        {
            int _packetLength = 0;

            receivedData.SetBytes(_data);

            if (receivedData.UnreadLength() >= 4)
            {
                _packetLength = receivedData.ReadInt();
                if (_packetLength <= 0)
                {
                    return true;
                }
            }

            
            while (_packetLength > 0 && _packetLength <= receivedData.UnreadLength())
            {
                byte[] _packetBytes = receivedData.ReadBytes(_packetLength);
                
                ThreadManager.ExecuteOnMainThread(() =>
                {
                    
                    using (Packet _packet = new Packet(_packetBytes))
                    {
                        
                        int _packetId = _packet.ReadInt();
                        
                        Server.packetHandlers[_packetId](id, _packet);
                    }
                });

                _packetLength = 0;
                if (receivedData.UnreadLength() >= 4)
                {
                    _packetLength = receivedData.ReadInt();
                    if (_packetLength <= 0)
                    {
                        return true;
                    }
                }
            }

            if (_packetLength <= 1)
            {
                return true;
            }

            return false;
        }

        public void Disconnect()
        {
            socket.Close();
            stream = null;
            receivedData = null;
            receiveBuffer = null;
            socket = null;
        }
    }

    public class UDP
    {
        public IPEndPoint endPoint;

        private int id;

        public UDP(int _id)
        {
            id = _id;
        }

        public void Connect(IPEndPoint _endPoint)
        {
            endPoint = _endPoint;
            ServerSend.UDPTest(id);
        }

        public void SendData(Packet _packet)
        {
            Server.SendUDPData(endPoint, _packet);
        }

        public void HandleData(Packet _packetData)
        {
            int _packetLength = _packetData.ReadInt();
            byte[] _packetBytes = _packetData.ReadBytes(_packetLength);

            ThreadManager.ExecuteOnMainThread(() =>
            {
                using (Packet _packet = new Packet(_packetBytes))
                {
                    int _packetId = _packet.ReadInt();
                    Server.packetHandlers[_packetId](id, _packet);
                }
            });
        }

        public void Disconnect()
        {
            endPoint = null;
        }
    }

    public void SendIntoGame(User _user)
    {
        user = _user;
        player = NetworkManager.instance.InstantiatePlayer();
        player.Initialize(id, _user.username, new Vector3(0, 0, 0));

        foreach (Client _client in Server.clients.Values)
        {
            if(_client.player != null)
            {
                if(_client.id != id)
                {
                    ServerSend.playerSpawn(id, _client.player);
                }
            }
        }

        foreach (Client _client in Server.clients.Values)
        {
            if(_client.player != null)
            {
                ServerSend.playerSpawn(_client.id, player);
            }
        }
    }

    private void Disconnect()
    {
        Debug.Log($"{tcp.socket.Client.RemoteEndPoint} has disconnected.");

        ThreadManager.ExecuteOnMainThread(() =>
        {
            username = "";
            
            if(player != null)
            {
                UnityEngine.Object.Destroy(player.gameObject);
                player = null;
            }
        });

        tcp.Disconnect();
        udp.Disconnect();

        ServerSend.PlayerDisconnected(id);
    }
}
```

**Packet.cs** <br>
intialize enumerator used in the package to be sent <br>
```
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

/// <summary>Sent from server to client.</summary>
public enum ServerPackets
{
    tcpTest = 1,
    udpTest,
    login,
    signUp,
    playerConnect,
    playerDisconnected,
    spawnFood,
    destroyFood,
    playerSpawn,
    playerPosition,
    playerRotation
}

/// <summary>Sent from client to server.</summary>
public enum ClientPackets
{
    startUp = 1,
    loginInput,
    signUpInput,
    playerMovement,
    playerEat
}

public class Packet : IDisposable
{
    private List<byte> buffer;
    private byte[] readableBuffer;
    private int readPos;

    /// <summary>Creates a new empty packet (without an ID).</summary>
    public Packet()
    {
        buffer = new List<byte>(); // Intitialize buffer
        readPos = 0; // Set readPos to 0
    }

    /// <summary>Creates a new packet with a given ID. Used for sending.</summary>
    /// <param name="_id">The packet ID.</param>
    public Packet(int _id)
    {
        buffer = new List<byte>(); // Intitialize buffer
        readPos = 0; // Set readPos to 0

        Write(_id); // Write packet id to the buffer
    }

    /// <summary>Creates a packet from which data can be read. Used for receiving.</summary>
    /// <param name="_data">The bytes to add to the packet.</param>
    public Packet(byte[] _data)
    {
        buffer = new List<byte>(); // Intitialize buffer
        readPos = 0; // Set readPos to 0

        SetBytes(_data);
    }

    #region Functions
    /// <summary>Sets the packet's content and prepares it to be read.</summary>
    /// <param name="_data">The bytes to add to the packet.</param>
    public void SetBytes(byte[] _data)
    {
        Write(_data);
        readableBuffer = buffer.ToArray();
    }

    /// <summary>Inserts the length of the packet's content at the start of the buffer.</summary>
    public void WriteLength()
    {
        buffer.InsertRange(0, BitConverter.GetBytes(buffer.Count)); // Insert the byte length of the packet at the very beginning
    }

    /// <summary>Inserts the given int at the start of the buffer.</summary>
    /// <param name="_value">The int to insert.</param>
    public void InsertInt(int _value)
    {
        buffer.InsertRange(0, BitConverter.GetBytes(_value)); // Insert the int at the start of the buffer
    }

    /// <summary>Gets the packet's content in array form.</summary>
    public byte[] ToArray()
    {
        readableBuffer = buffer.ToArray();
        return readableBuffer;
    }

    /// <summary>Gets the length of the packet's content.</summary>
    public int Length()
    {
        return buffer.Count; // Return the length of buffer
    }

    /// <summary>Gets the length of the unread data contained in the packet.</summary>
    public int UnreadLength()
    {
        return Length() - readPos; // Return the remaining length (unread)
    }

    /// <summary>Resets the packet instance to allow it to be reused.</summary>
    /// <param name="_shouldReset">Whether or not to reset the packet.</param>
    public void Reset(bool _shouldReset = true)
    {
        if (_shouldReset)
        {
            buffer.Clear(); // Clear buffer
            readableBuffer = null;
            readPos = 0; // Reset readPos
        }
        else
        {
            readPos -= 4; // "Unread" the last read int
        }
    }
    #endregion

    #region Write Data
    /// <summary>Adds a byte to the packet.</summary>
    /// <param name="_value">The byte to add.</param>
    public void Write(byte _value)
    {
        buffer.Add(_value);
    }
    /// <summary>Adds an array of bytes to the packet.</summary>
    /// <param name="_value">The byte array to add.</param>
    public void Write(byte[] _value)
    {
        buffer.AddRange(_value);
    }
    /// <summary>Adds a short to the packet.</summary>
    /// <param name="_value">The short to add.</param>
    public void Write(short _value)
    {
        buffer.AddRange(BitConverter.GetBytes(_value));
    }
    /// <summary>Adds an int to the packet.</summary>
    /// <param name="_value">The int to add.</param>
    public void Write(int _value)
    {
        buffer.AddRange(BitConverter.GetBytes(_value));
    }
    /// <summary>Adds a long to the packet.</summary>
    /// <param name="_value">The long to add.</param>
    public void Write(long _value)
    {
        buffer.AddRange(BitConverter.GetBytes(_value));
    }
    /// <summary>Adds a float to the packet.</summary>
    /// <param name="_value">The float to add.</param>
    public void Write(float _value)
    {
        buffer.AddRange(BitConverter.GetBytes(_value));
    }
    /// <summary>Adds a bool to the packet.</summary>
    /// <param name="_value">The bool to add.</param>
    public void Write(bool _value)
    {
        buffer.AddRange(BitConverter.GetBytes(_value));
    }
    /// <summary>Adds a string to the packet.</summary>
    /// <param name="_value">The string to add.</param>
    public void Write(string _value)
    {
        Write(_value.Length); // Add the length of the string to the packet
        buffer.AddRange(Encoding.ASCII.GetBytes(_value)); // Add the string itself
    }
    /// <summary>Adds a Vector3 to the packet.</summary>
    /// <param name="_value">The Vector3 to add.</param>
    public void Write(Vector3 _value)
    {
        Write(_value.x);
        Write(_value.y);
        Write(_value.z);
    }
    /// <summary>Adds a Quaternion to the packet.</summary>
    /// <param name="_value">The Quaternion to add.</param>
    public void Write(Quaternion _value)
    {
        Write(_value.x);
        Write(_value.y);
        Write(_value.z);
        Write(_value.w);
    }
    #endregion

    #region Read Data
    /// <summary>Reads a byte from the packet.</summary>
    /// <param name="_moveReadPos">Whether or not to move the buffer's read position.</param>
    public byte ReadByte(bool _moveReadPos = true)
    {
        if (buffer.Count > readPos)
        {
            // If there are unread bytes
            byte _value = readableBuffer[readPos]; // Get the byte at readPos' position
            if (_moveReadPos)
            {
                // If _moveReadPos is true
                readPos += 1; // Increase readPos by 1
            }
            return _value; // Return the byte
        }
        else
        {
            throw new Exception("Could not read value of type 'byte'!");
        }
    }

    /// <summary>Reads an array of bytes from the packet.</summary>
    /// <param name="_length">The length of the byte array.</param>
    /// <param name="_moveReadPos">Whether or not to move the buffer's read position.</param>
    public byte[] ReadBytes(int _length, bool _moveReadPos = true)
    {
        if (buffer.Count > readPos)
        {
            // If there are unread bytes
            byte[] _value = buffer.GetRange(readPos, _length).ToArray(); // Get the bytes at readPos' position with a range of _length
            if (_moveReadPos)
            {
                // If _moveReadPos is true
                readPos += _length; // Increase readPos by _length
            }
            return _value; // Return the bytes
        }
        else
        {
            throw new Exception("Could not read value of type 'byte[]'!");
        }
    }

    /// <summary>Reads a short from the packet.</summary>
    /// <param name="_moveReadPos">Whether or not to move the buffer's read position.</param>
    public short ReadShort(bool _moveReadPos = true)
    {
        if (buffer.Count > readPos)
        {
            // If there are unread bytes
            short _value = BitConverter.ToInt16(readableBuffer, readPos); // Convert the bytes to a short
            if (_moveReadPos)
            {
                // If _moveReadPos is true and there are unread bytes
                readPos += 2; // Increase readPos by 2
            }
            return _value; // Return the short
        }
        else
        {
            throw new Exception("Could not read value of type 'short'!");
        }
    }

    /// <summary>Reads an int from the packet.</summary>
    /// <param name="_moveReadPos">Whether or not to move the buffer's read position.</param>
    public int ReadInt(bool _moveReadPos = true)
    {
        if (buffer.Count > readPos)
        {
            // If there are unread bytes
            int _value = BitConverter.ToInt32(readableBuffer, readPos); // Convert the bytes to an int
            if (_moveReadPos)
            {
                // If _moveReadPos is true
                readPos += 4; // Increase readPos by 4
            }
            return _value; // Return the int
        }
        else
        {
            throw new Exception("Could not read value of type 'int'!");
        }
    }

    /// <summary>Reads a long from the packet.</summary>
    /// <param name="_moveReadPos">Whether or not to move the buffer's read position.</param>
    public long ReadLong(bool _moveReadPos = true)
    {
        if (buffer.Count > readPos)
        {
            // If there are unread bytes
            long _value = BitConverter.ToInt64(readableBuffer, readPos); // Convert the bytes to a long
            if (_moveReadPos)
            {
                // If _moveReadPos is true
                readPos += 8; // Increase readPos by 8
            }
            return _value; // Return the long
        }
        else
        {
            throw new Exception("Could not read value of type 'long'!");
        }
    }

    /// <summary>Reads a float from the packet.</summary>
    /// <param name="_moveReadPos">Whether or not to move the buffer's read position.</param>
    public float ReadFloat(bool _moveReadPos = true)
    {
        if (buffer.Count > readPos)
        {
            // If there are unread bytes
            float _value = BitConverter.ToSingle(readableBuffer, readPos); // Convert the bytes to a float
            if (_moveReadPos)
            {
                // If _moveReadPos is true
                readPos += 4; // Increase readPos by 4
            }
            return _value; // Return the float
        }
        else
        {
            throw new Exception("Could not read value of type 'float'!");
        }
    }

    /// <summary>Reads a bool from the packet.</summary>
    /// <param name="_moveReadPos">Whether or not to move the buffer's read position.</param>
    public bool ReadBool(bool _moveReadPos = true)
    {
        if (buffer.Count > readPos)
        {
            // If there are unread bytes
            bool _value = BitConverter.ToBoolean(readableBuffer, readPos); // Convert the bytes to a bool
            if (_moveReadPos)
            {
                // If _moveReadPos is true
                readPos += 1; // Increase readPos by 1
            }
            return _value; // Return the bool
        }
        else
        {
            throw new Exception("Could not read value of type 'bool'!");
        }
    }

    /// <summary>Reads a string from the packet.</summary>
    /// <param name="_moveReadPos">Whether or not to move the buffer's read position.</param>
    public string ReadString(bool _moveReadPos = true)
    {
        try
        {
            int _length = ReadInt(); // Get the length of the string
            string _value = Encoding.ASCII.GetString(readableBuffer, readPos, _length); // Convert the bytes to a string
            if (_moveReadPos && _value.Length > 0)
            {
                // If _moveReadPos is true string is not empty
                readPos += _length; // Increase readPos by the length of the string
            }
            return _value; // Return the string
        }
        catch
        {
            throw new Exception("Could not read value of type 'string'!");
        }
    }

    /// <summary>Reads a Vector3 from the packet.</summary>
    /// <param name="_moveReadPos">Whether or not to move the buffer's read position.</param>
    public Vector3 ReadVector3(bool _moveReadPos = true)
    {
        return new Vector3(ReadFloat(_moveReadPos), ReadFloat(_moveReadPos), ReadFloat(_moveReadPos));
    }

    /// <summary>Reads a Quaternion from the packet.</summary>
    /// <param name="_moveReadPos">Whether or not to move the buffer's read position.</param>
    public Quaternion ReadQuaternion(bool _moveReadPos = true)
    {
        return new Quaternion(ReadFloat(_moveReadPos), ReadFloat(_moveReadPos), ReadFloat(_moveReadPos), ReadFloat(_moveReadPos));
    }
    #endregion

    private bool disposed = false;

    protected virtual void Dispose(bool _disposing)
    {
        if (!disposed)
        {
            if (_disposing)
            {
                buffer = null;
                readableBuffer = null;
                readPos = 0;
            }

            disposed = true;
        }
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
}
```

**Server.cs** <br>
set up the port and max player <br>
```
using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using UnityEngine;
using System.IO;

public class Server
{
    public static int MaxPlayers { get; private set; }
    public static int Port { get; private set; }

    public static Dictionary<int, Client> clients = new Dictionary<int, Client>();
    public delegate void PacketHandler(int _fromClient, Packet _packet);
    public static Dictionary<int, PacketHandler> packetHandlers;

    private static TcpListener tcpListener;
    private static UdpClient udpListener;

    public static void Start(int _maxPlayers, int _port)
    {
        MaxPlayers = _maxPlayers;
        Port = _port;
        
        InitializeServerData();

        tcpListener = new TcpListener(IPAddress.Any, Port);
        tcpListener.Start();
        tcpListener.BeginAcceptTcpClient(TCPConnectCallback, null);

        udpListener = new UdpClient(Port);
        udpListener.BeginReceive(UDPReceiveCallback, null);

        Debug.Log($"Server started on port {Port}.");
    }

    private static void TCPConnectCallback(IAsyncResult _result)
    {
        TcpClient _client = tcpListener.EndAcceptTcpClient(_result);
        tcpListener.BeginAcceptTcpClient(TCPConnectCallback, null);
        Debug.Log($"Incoming connection from {_client.Client.RemoteEndPoint}...");

        for (int i = 1; i <= MaxPlayers; i++)
        {
            if (clients[i].tcp.socket == null)
            {
                clients[i].tcp.Connect(_client);
                return;
            }
        }

        Debug.Log($"{_client.Client.RemoteEndPoint} failed to connect: Server full!");
    }

    private static void UDPReceiveCallback(IAsyncResult _result)
    {
        try
        {
            IPEndPoint _clientEndPoint = new IPEndPoint(IPAddress.Any, 0);
            byte[] _data = udpListener.EndReceive(_result, ref _clientEndPoint);
            udpListener.BeginReceive(UDPReceiveCallback, null);

            if (_data.Length < 4)
            {
                return;
            }

            using (Packet _packet = new Packet(_data))
            {
                int _clientId = _packet.ReadInt();

                if (_clientId == 0)
                {
                    return;
                }

                if (clients[_clientId].udp.endPoint == null)
                {
                    clients[_clientId].udp.Connect(_clientEndPoint);
                    return;
                }

                if (clients[_clientId].udp.endPoint.ToString() == _clientEndPoint.ToString())
                {
                    clients[_clientId].udp.HandleData(_packet);
                }
            }
        }
        catch (Exception _ex)
        {
            Debug.Log($"Error receiving UDP data: {_ex}");
        }
    }

    public static void SendUDPData(IPEndPoint _clientEndPoint, Packet _packet)
    {
        try
        {
            if (_clientEndPoint != null)
            {
                udpListener.BeginSend(_packet.ToArray(), _packet.Length(), _clientEndPoint, null, null);
            }
        }
        catch (Exception _ex)
        {
            Debug.Log($"Error sending data to {_clientEndPoint} via UDP: {_ex}");
        }
    }

    public static void CloseSocket()
    {
        tcpListener.Stop();
        udpListener.Close();
    }

    private static void InitializeServerData()
    {   
        for (int i = 1; i <= MaxPlayers; i++)
        {
            clients.Add(i, new Client(i));
        }

        packetHandlers = new Dictionary<int, PacketHandler>()
        {
            { (int)ClientPackets.startUp, ServerHandle.startUp },
            { (int)ClientPackets.loginInput, ServerHandle.LoginInput },
            { (int)ClientPackets.signUpInput, ServerHandle.SignUpInput },
            { (int)ClientPackets.playerMovement, ServerHandle.PlayerMovement },
        };
    }
}
```

**ServerHandle.cs** <br>
server will perform according to the received packet <br>
```
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerHandle
{
    public static void startUp(int _fromClient, Packet _packet)
    {
        int _clientCheck = _packet.ReadInt();
        Debug.Log($"player {_fromClient} connected successfully from {Server.clients[_fromClient].tcp.socket.Client.RemoteEndPoint}");
        if (_fromClient != _clientCheck)
        {
            Debug.Log($"Player ID: {_fromClient} has assumed the wrong client ID ({_clientCheck})!");
        }
    }

    public static void LoginInput(int _fromClient, Packet _packet)
    {
        string _nickname = _packet.ReadString();
        string _password = _packet.ReadString();

        AuthManager.instance.LoginInput(_fromClient, _nickname, _password);
    }

    public static void SignUpInput(int _fromClient, Packet _packet)
    {
        string _nickname = _packet.ReadString();
        string _password = _packet.ReadString();

        AuthManager.instance.SignUpInput(_fromClient, _nickname, _password);
    }

    public static void PlayerMovement(int _fromClient, Packet _packet)
    {
        bool[] _inputs = new bool[_packet.ReadInt()];
        for (int i = 0; i < _inputs.Length; i++)
        {
            _inputs[i] = _packet.ReadBool();
        }
        Quaternion _rotation = _packet.ReadQuaternion();

        Server.clients[_fromClient].player.SetInput(_inputs, _rotation);
    }


}
```

**ServerSend.cs** <br>
send package to client <br>
```
using System.Text;
using UnityEngine;

public class ServerSend
{
    private static void SendTCPData(int _toClient, Packet _packet)
    {
        _packet.WriteLength();
        Server.clients[_toClient].tcp.SendData(_packet);
    }

    private static void SendUDPData(int _toClient, Packet _packet)
    {
        _packet.WriteLength();
        Server.clients[_toClient].udp.SendData(_packet);
    }

    private static void SendTCPDataToAll(Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            Server.clients[i].tcp.SendData(_packet);
        }
    }

    private static void SendTCPDataToAll(int _exceptClient, Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            if (i != _exceptClient)
            {
                Server.clients[i].tcp.SendData(_packet);
            }
        }
    }

    private static void SendUDPDataToAll(Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            Server.clients[i].udp.SendData(_packet);
        }
    }

    private static void SendUDPDataToAll(int _exceptClient, Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            if (i != _exceptClient)
            {
                Server.clients[i].udp.SendData(_packet);
            }
        }
    }

    #region Packets
    public static void TCPTest(int _toClient)
    {
        using (Packet _packet = new Packet((int)ServerPackets.tcpTest))
        {
            _packet.Write(_toClient);

            SendTCPData(_toClient, _packet);
        }
    }

    public static void UDPTest(int _toClient)
    {
        using (Packet _packet = new Packet((int)ServerPackets.udpTest))
        {
            SendUDPData(_toClient, _packet);
        }
    }

    public static void Login(int _id, bool _value, string _message)
    {
        using (Packet _packet = new Packet((int)ServerPackets.login))
        {
            _packet.Write(_value);
            _packet.Write(_message);

            if(_value)
            {
                _packet.Write(Server.clients[_id].username);
                Debug.Log("Player id is logged in, welcome " + Server.clients[_id].username);
            }

            SendTCPData(_id, _packet);
        }
    }

    public static void SignUp(int _id, bool _value, string _message)
    {
        using (Packet _packet = new Packet((int)ServerPackets.signUp))
        {
            _packet.Write(_value);
            _packet.Write(_message);

            SendTCPData(_id, _packet);
        }
    }

    public static void PlayerDisconnected(int _id)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerDisconnected))
        {
            _packet.Write(_id);

            SendTCPDataToAll(_id, _packet);
        }
    }

    public static void PlayerConnect(int _id)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerConnect))
        {
            _packet.Write(_id);

            SendTCPDataToAll(_id, _packet);
        }
    }

    public static void playerSpawn(int _id, Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerSpawn))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.username);
            _packet.Write(_player.transform.position);
            _packet.Write(_player.transform.rotation);
            Debug.Log(_player.transform.position);
            SendTCPData(_id, _packet);
        }
    }

    public static void PlayerPosition(Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerPosition))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.transform.position);

            SendUDPDataToAll(_packet);
        }
    }

    public static void PlayerRotation(Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerRotation))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.transform.rotation);

            SendUDPDataToAll(_packet);
        }
    }
    public static void spawnFood(int _foodId, Vector3 _position)
    {
        using (Packet _packet = new Packet((int)ServerPackets.spawnFood))
        {
            _packet.Write(_foodId);
            _packet.Write(_position);
            SendTCPDataToAll(_packet);
        }
    }

    public static void destroyFood(int _foodId)
    {
        using (Packet _packet = new Packet((int)ServerPackets.destroyFood))
        {
            _packet.Write(_foodId);
            SendTCPDataToAll(_packet);
        }
    }
    #endregion
}
```

**AuthManager.cs** <br>
check the login/signup input from client then send the respon <br>
```
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AuthManager : MonoBehaviour
{
    public static AuthManager instance;

    public List<User> users = new List<User>(); 

    private AuthError error;

    private enum AuthError
    {
        None,
        UsernameAlreadyExist,
        UserNotFound,
        WrongPassword
    } 

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void LoginInput(int _clientId, string _username, string _password)
    {
        User _user = Authenticate(_username, _password);     
        
        if(error != AuthError.None)
        {
            string _message = "Login Failed!";
            switch(error)
            {
                case AuthError.WrongPassword:
                    _message = "Wrong password";
                    break;
                case AuthError.UserNotFound:
                    _message = "Account does not exist";
                    break;
            }
            ServerSend.Login(_clientId, false, _message);
        }
        else
        {
            Server.clients[_clientId].username = _username;

            string _message = "Login Success";
            ServerSend.Login(_clientId, true, _message);
            Server.clients[_clientId].SendIntoGame(_user);
        }
    }

    public void SignUpInput(int _clientId, string _username, string _password)
    {
        CreateNewUser(_username, _password);
        
        if(error != AuthError.None)
        {
            string _message = "Sign Up Failed!";
            switch(error)
            {
                case AuthError.UsernameAlreadyExist:
                    _message = "Username already exist";
                    break;
            }
            ServerSend.SignUp(_clientId, false, _message);
        }
        else
        {
            string _message = "Sign Up Success";
            ServerSend.SignUp(_clientId, true, _message);
        }
        
    }

    public User Authenticate(string _username, string _password)
    {
        foreach(User _user in users)
        {
            if(_username == _user.username)
            {
                if(_password != _user.password)
                {
                    error = AuthError.WrongPassword;
                    return null;
                }
                else
                {
                    error = AuthError.None;
                    return _user;
                }
            }
        }

        error = AuthError.UserNotFound;
        return null;
    }

    public void CreateNewUser(string _username, string _password)
    {
        foreach(User _user in users)
        {
            if(_username == _user.username)
            {
                error = AuthError.UsernameAlreadyExist;
                return;
            }
        }

        User _newUser = new User(_username, _password);
        users.Add(_newUser);

        error = AuthError.None;
        return;
    }
}
```

**Food.cs** <br>
initialize food <br>
```
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour
{
    public int foodId;
    private float destroyTime = 10f;

    // Start is called before the first frame update
    public void Initialize(int _foodId)
    {
        foodId = _foodId;
        StartCoroutine(Destroy());
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.transform.parent.gameObject.GetComponent<Player>().AddFood();
            destroyFood();
        }
    }
    private void destroyFood()
    {
        ServerSend.destroyFood(foodId);
        SpawnFood.food.Remove(foodId);
        Destroy(gameObject);
    }

    IEnumerator Destroy()
    {
        yield return new WaitForSeconds(destroyTime);
        destroyFood();
    }

}
```

**NetworkManager.cs** <br>
to start the server and spawn player in server <br>
```
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NetworkManager : MonoBehaviour
{
    public static NetworkManager instance;

    [Header("Server Settings")]
    public int maxPlayers;
    public int port;

    [Header("Prefabs")]
    public GameObject playerPrefabs;
    
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else 
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        //Limit the application to reduce memory usage
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 30;

        Server.Start(maxPlayers, port);
    }

    private void OnApplicationQuit()
    {
        Server.CloseSocket();
    }

    public Player InstantiatePlayer()
    {
        int posx = Random.Range(0, 5);
        int posy = Random.Range(0, 5);
        Player player = Instantiate(playerPrefabs, new Vector3(posx, posy ,0), Quaternion.identity).GetComponent<Player>();
        Debug.Log(player);
       // Debug.Log(posx);
       // Debug.Log(posy);
        player.transform.SetParent(this.transform, false);
        return player;
    }
}
```


**Player.cs** <br>
receive player's input from client <br>
```
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Pipes;
using System.Text;
using System;

public class Player : MonoBehaviour
{
    /*public int id;
    public string username;

    private void LateUpdate()
    {        
        //PlayerMovement();
    }

    public void Initialize(int _id)
    {
        id = _id;
        username = Server.clients[_id].username;
    }

    public void PlayerMovement()
    {
        //ServerSend.SendPlayerPosition(this);
        //ServerSend.SendPlayerRotation(this);
    }*/

    public int id;
    public int food;
    public string username;

    private float moveSpeed = 10f / Constants.TICKS_PER_SEC;
    private bool[] inputs;

    public void Initialize(int _id, string _username, Vector3 _spawnPosition)
    {
        id = _id;
        username = _username;

        inputs = new bool[4];
    }

    private void Start()
    {
       
    }

    private void FixedUpdate()
    {
        Vector2 _inputDirection = Vector2.zero;
        if (inputs[0])
        {
            _inputDirection.y += 1;
        }
        if (inputs[1])
        {
            _inputDirection.x -= 1;
        }
        if (inputs[2])
        {
            _inputDirection.y -= 1;
        }
        if (inputs[3])
        {
            _inputDirection.x += 1;
        }

        Move(_inputDirection);
        ServerSend.PlayerPosition(this);
        ServerSend.PlayerRotation(this);
    }

    private void Move(Vector2 _inputDirection)
    {
            Vector3 _moveDirection = transform.right * _inputDirection.x + transform.up * _inputDirection.y;
            transform.position += _moveDirection * moveSpeed;
        ServerSend.PlayerPosition(this);
        ServerSend.PlayerRotation(this);
    }

    public void SetInput(bool[] _inputs, Quaternion _rotation)
    {
        inputs = _inputs;
        transform.rotation = _rotation;
    }

    public void AddFood()
    {
        food++;
        //ServerSend.PlayerCoin(id, food);
    }

}
```

**SpawnFood.cs** <br>
spawn food and send the coordinate to client <br>
```
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFood : MonoBehaviour
{
    public static Dictionary<int, Food> food = new Dictionary<int, Food>();
    private int idCount;

    public Vector3 spawnValues;

    public float spawnMostWait;
    public float spawnLeastWait;
    private float spawnWait;

    [Header("Prefab")]
    public Food foodPrefab;

    private void Start()
    {
        spawnWait = Random.Range(spawnLeastWait, spawnMostWait);
    }

    private void Update()
    {
        if (spawnWait > 0)
        {
            spawnWait -= Time.deltaTime;
        }
        else
        {
            Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), Random.Range(-spawnValues.y, spawnValues.y), 0);
            spawnPosition += this.transform.position;

            ServerSend.spawnFood(idCount, spawnPosition);
            Food food = Instantiate(foodPrefab, spawnPosition, foodPrefab.transform.rotation) as Food;

            food.Initialize(idCount);
            idCount++;

            //food.Add(idCount, food);

            spawnWait = Random.Range(spawnLeastWait, spawnMostWait);
        }

    }

}
```


