﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public Dictionary<int, Player> players = new Dictionary<int, Player>();
    public static Dictionary<int, Food> foods = new Dictionary<int, Food>();

    public Food foodPrefab;

    public GameObject localPrefab, playerPrefab;

    // Start is called before the first frame update
    void Start()
    {
        Client.instance.ConnectToServer();
    }

    public void SpawnPlayer(int _id, string _username, Vector3 position, Quaternion rotation)
    {
        GameObject _player;
        if(_id == Client.instance.myId)
        {
            _player = Instantiate(localPrefab, position, rotation);
        }
        else
        {
            _player = Instantiate(playerPrefab, position, rotation);
        }

        _player.transform.SetParent(this.transform, false);
        _player.GetComponent<Player>().Initialize(_id, _username);
        //_player.GetComponent<Player>().id = _id;
        //_player.GetComponent<Player>().username = _username;
        players.Add(_id, _player.GetComponent<Player>());
    }
    
    public void SpawnFood(int _foodId, Vector3 _position)
    {
        Food food = Instantiate(foodPrefab, _position, foodPrefab.transform.rotation) as Food;
        food.Initialize(_foodId);
        foods.Add(_foodId, food);
    }

    public void FoodDestroy(int _foodId)
    {
            Destroy(foods[_foodId].gameObject);
            foods.Remove(_foodId);
    }
}
