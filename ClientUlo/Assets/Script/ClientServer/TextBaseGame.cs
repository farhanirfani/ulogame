﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class TextBaseGame : MonoBehaviour
{
    private string posFromServer;
    Position pos;
    public Text posText;

    public int initPosX, initPosY;
    // Start is called before the first frame update
    void Start()
    {
        initPosX = 0;
        initPosY = 0;
        posText.text = "Client Position : " + initPosX.ToString() + " , " + initPosY.ToString();

        try
        {
            ConnectionDDOL.sr = new StreamReader(ConnectionDDOL.client.GetStream());
            ConnectionDDOL.sw = new StreamWriter(ConnectionDDOL.client.GetStream());
            Thread t = new Thread(ReceivePosition);
            t.Start();
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //posText.text = posFromServer;
    }

    public void MoveUp()
    {
        Position pos = new Position(initPosX, initPosY);
        ConnectionDDOL.ns = ConnectionDDOL.client.GetStream();
        ConnectionDDOL.sw.WriteLine("Up");
        ConnectionDDOL.sw.Flush();
        IFormatter formatter = new BinaryFormatter();
        formatter.Binder = new CustomizedBinder();
        formatter.Serialize(ConnectionDDOL.ns, pos);
    }

    public void MoveDown()
    {
        Position pos = new Position(initPosX, initPosY);
        ConnectionDDOL.ns = ConnectionDDOL.client.GetStream();
        ConnectionDDOL.sw.WriteLine("Down");
        ConnectionDDOL.sw.Flush();
        IFormatter formatter = new BinaryFormatter();
        formatter.Binder = new CustomizedBinder();
        formatter.Serialize(ConnectionDDOL.ns, pos);
    }
    public void MoveLeft()
    {
        Position pos = new Position(initPosX, initPosY);
        ConnectionDDOL.ns = ConnectionDDOL.client.GetStream();
        ConnectionDDOL.sw.WriteLine("Left");
        ConnectionDDOL.sw.Flush();
        IFormatter formatter = new BinaryFormatter();
        formatter.Binder = new CustomizedBinder();
        formatter.Serialize(ConnectionDDOL.ns, pos);
    }
    public void MoveRight()
    {
        Position pos = new Position(initPosX, initPosY);
        ConnectionDDOL.ns = ConnectionDDOL.client.GetStream();
        ConnectionDDOL.sw.WriteLine("Right");
        ConnectionDDOL.sw.Flush();
        IFormatter formatter = new BinaryFormatter();
        formatter.Binder = new CustomizedBinder();
        formatter.Serialize(ConnectionDDOL.ns, pos);    
    }

    private void UpdateY()
    {
        ConnectionDDOL.ns = ConnectionDDOL.client.GetStream();
        IFormatter formatter = new BinaryFormatter();
        formatter.Binder = new CustomizedBinder();
        pos = (Position)formatter.Deserialize(ConnectionDDOL.ns);
        initPosY = pos.PositionY;
        posText.text = "Client Position : " + initPosX.ToString() + " , " + initPosY.ToString();
        Debug.Log("Player Moving Up " + initPosY);
    }

    private void UpdateX()
    {
        ConnectionDDOL.ns = ConnectionDDOL.client.GetStream();
        IFormatter formatter = new BinaryFormatter();
        formatter.Binder = new CustomizedBinder();
        pos = (Position)formatter.Deserialize(ConnectionDDOL.ns);
        initPosX = pos.PositionX;
        posText.text = "Client Position : " + initPosX.ToString() + " , " + initPosY.ToString();
        Debug.Log("Player Moving Up " + initPosX);
    }

    void ReceivePosition()
    {
        while (true)
        {
            posFromServer = ConnectionDDOL.sr.ReadLine();
            Debug.Log(posFromServer);
            if (posFromServer.Contains("Up"))
            {
                UpdateY();
            }
            else if (posFromServer.Contains("Down"))
            {
                UpdateY();
            }
            else if (posFromServer.Contains("Left"))
            {
                UpdateX();
            }
            else if (posFromServer.Contains("Right"))
            {
                UpdateX();
            }

        }
    }

    sealed class CustomizedBinder : SerializationBinder
    {
        public override Type BindToType(string assemblyName, string typeName)
        {
            Type returntype = null;
            string sharedAssemblyName = "SharedAssembly, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
            assemblyName = Assembly.GetExecutingAssembly().FullName;
            typeName = typeName.Replace(sharedAssemblyName, assemblyName);
            returntype = Type.GetType(String.Format("{0}, {1}", typeName, assemblyName));

            return returntype;
        }

        public override void BindToName(Type serializedType, out string assemblyName, out string typeName)
        {
            base.BindToName(serializedType, out assemblyName, out typeName);
            assemblyName = "SharedAssembly, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
        }
    }
}
