﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using UnityEngine;

public class ConnectionDDOL : MonoBehaviour
{
    static ConnectionDDOL instance;

    private static string Address = "127.0.0.1";
    private static int Port = 5000;

    public static NetworkStream ns;
    public static StreamWriter sw;
    public static StreamReader sr;
    public static TcpClient client;
    // Start is called before the first frame update

    void Awake()
    {
        if (instance == null)
        {
            instance = this; // In first scene, make us the singleton.
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
            Destroy(gameObject); // On reload, singleton already set, so destroy duplicate.

        client = new TcpClient(Address, Port);

    }

    private void OnDisable()
    {
        ns.Close();
        sw.Close();
        client.Close();
    }
}