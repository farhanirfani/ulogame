﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    public static bool isWin;
    public GameObject winText, loseText;
    // Start is called before the first frame update
    void Start()
    {
        if (isWin)
        {
            winText.SetActive(true);
        } else
        {
            loseText.SetActive(false);  
        }
    }

    public void BackButton()
    {
        SceneManager.LoadScene("Menu");
    }

    public void ExitButton()
    {
        Application.Quit();
    }

}
