﻿using System;
using System.Net;
using UnityEngine;

public class ClientHandle : MonoBehaviour
{
    public static void TcpTest(Packet _packet)
    {
        int _myId = _packet.ReadInt(); 
        Client.instance.myId = _myId;
        Debug.Log("TCP Connected");

        ClientSend.TCPTestReceived();
        Client.instance.udp.Connect(((IPEndPoint)Client.instance.tcp.socket.Client.LocalEndPoint).Port);
    }

    public static void UdpTest(Packet _packet)
    {
        Debug.Log("UDP Connected");
    }

    public static void Login(Packet _packet)
    {
        bool _success = _packet.ReadBool();
        string _message = _packet.ReadString();

        if(_success)
        {
            string _username = _packet.ReadString();
            Client.instance.username = _username;
            AuthManager.instance.playButton.interactable = true;
            //LoadScene("Game");
        }
        
        AuthManager.instance.statusText.text = _message;
    }

    public static void SignUp(Packet _packet)
    {
        bool success = _packet.ReadBool();
        string _message = _packet.ReadString();

        if (success)
        {
            AuthManager.instance.ResetInput();
        }
        AuthManager.instance.statusText.text = _message;
    }

    public static void playerSpawn(Packet _packet)
    {
        int _id = _packet.ReadInt();
        string _username = _packet.ReadString();
        Vector3 position = _packet.ReadVector3();
        Quaternion rotation = _packet.ReadQuaternion();
        Debug.Log(position);
        GameManager.instance.SpawnPlayer(_id, _username, position, rotation);
    }

    public static void PlayerPosition(Packet _packet)
    {
        int _id = _packet.ReadInt();
        if (GameManager.instance.players.ContainsKey(_id))
        {
            Vector3 _position = _packet.ReadVector3();
            //Debug.Log(_position);
            GameManager.instance.players[_id].transform.position = _position;
        }

    }

    public static void PlayerRotation(Packet _packet)
    {
        int _id = _packet.ReadInt();
        if (GameManager.instance.players.ContainsKey(_id))
        {
            Quaternion _rotation = _packet.ReadQuaternion();

            GameManager.instance.players[_id].transform.rotation = _rotation;
        }
    }

    public static void PlayerDisconnected(Packet _packet)
    {
        string _id = _packet.ReadString();

        Debug.Log("player " + _id + " is disconnected");
    }

    public static void SpawnFood(Packet _packet)
    {
        int _foodId = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();

        GameManager.instance.SpawnFood(_foodId, _position);
    }

    public static void DestroyFood(Packet _packet)
    {
        int _foodId = _packet.ReadInt();
        GameManager.instance.FoodDestroy(_foodId);
    }
}
