using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NetworkManager : MonoBehaviour
{
    public static NetworkManager instance;

    [Header("Server Settings")]
    public int maxPlayers;
    public int port;

    [Header("Prefabs")]
    public GameObject playerPrefabs;
    
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else 
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        //Limit the application to reduce memory usage
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 30;

        Server.Start(maxPlayers, port);
    }

    private void OnApplicationQuit()
    {
        Server.CloseSocket();
    }

    public Player InstantiatePlayer()
    {
        int posx = Random.Range(0, 5);
        int posy = Random.Range(0, 5);
        Player player = Instantiate(playerPrefabs, new Vector3(posx, posy ,0), Quaternion.identity).GetComponent<Player>();
        Debug.Log(player);
       // Debug.Log(posx);
       // Debug.Log(posy);
        player.transform.SetParent(this.transform, false);
        return player;
    }
}
