﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour
{
    public int foodId;
    private float destroyTime = 10f;

    // Start is called before the first frame update
    public void Initialize(int _foodId)
    {
        foodId = _foodId;
        StartCoroutine(Destroy());
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.transform.parent.gameObject.GetComponent<Player>().AddFood();
            destroyFood();
        }
    }
    private void destroyFood()
    {
        ServerSend.destroyFood(foodId);
        SpawnFood.food.Remove(foodId);
        Destroy(gameObject);
    }

    IEnumerator Destroy()
    {
        yield return new WaitForSeconds(destroyTime);
        destroyFood();
    }

}
