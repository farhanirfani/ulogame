using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Pipes;
using System.Text;
using System;

public class Player : MonoBehaviour
{
    /*public int id;
    public string username;

    private void LateUpdate()
    {        
        //PlayerMovement();
    }

    public void Initialize(int _id)
    {
        id = _id;
        username = Server.clients[_id].username;
    }

    public void PlayerMovement()
    {
        //ServerSend.SendPlayerPosition(this);
        //ServerSend.SendPlayerRotation(this);
    }*/

    public int id;
    public int food;
    public string username;

    private float moveSpeed = 10f / Constants.TICKS_PER_SEC;
    private bool[] inputs;

    public void Initialize(int _id, string _username, Vector3 _spawnPosition)
    {
        id = _id;
        username = _username;

        inputs = new bool[4];
    }

    private void Start()
    {
       
    }

    private void FixedUpdate()
    {
        Vector2 _inputDirection = Vector2.zero;
        if (inputs[0])
        {
            _inputDirection.y += 1;
        }
        if (inputs[1])
        {
            _inputDirection.x -= 1;
        }
        if (inputs[2])
        {
            _inputDirection.y -= 1;
        }
        if (inputs[3])
        {
            _inputDirection.x += 1;
        }

        Move(_inputDirection);
        ServerSend.PlayerPosition(this);
        ServerSend.PlayerRotation(this);
    }

    private void Move(Vector2 _inputDirection)
    {
            Vector3 _moveDirection = transform.right * _inputDirection.x + transform.up * _inputDirection.y;
            transform.position += _moveDirection * moveSpeed;
        ServerSend.PlayerPosition(this);
        ServerSend.PlayerRotation(this);
    }

    public void SetInput(bool[] _inputs, Quaternion _rotation)
    {
        inputs = _inputs;
        transform.rotation = _rotation;
    }

    public void AddFood()
    {
        food++;
        //ServerSend.PlayerCoin(id, food);
    }

}