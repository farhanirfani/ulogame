﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerHandle
{
    public static void startUp(int _fromClient, Packet _packet)
    {
        int _clientCheck = _packet.ReadInt();
        Debug.Log($"player {_fromClient} connected successfully from {Server.clients[_fromClient].tcp.socket.Client.RemoteEndPoint}");
        if (_fromClient != _clientCheck)
        {
            Debug.Log($"Player ID: {_fromClient} has assumed the wrong client ID ({_clientCheck})!");
        }
    }

    public static void LoginInput(int _fromClient, Packet _packet)
    {
        string _nickname = _packet.ReadString();
        string _password = _packet.ReadString();

        AuthManager.instance.LoginInput(_fromClient, _nickname, _password);
    }

    public static void SignUpInput(int _fromClient, Packet _packet)
    {
        string _nickname = _packet.ReadString();
        string _password = _packet.ReadString();

        AuthManager.instance.SignUpInput(_fromClient, _nickname, _password);
    }

    public static void PlayerMovement(int _fromClient, Packet _packet)
    {
        bool[] _inputs = new bool[_packet.ReadInt()];
        for (int i = 0; i < _inputs.Length; i++)
        {
            _inputs[i] = _packet.ReadBool();
        }
        Quaternion _rotation = _packet.ReadQuaternion();

        Server.clients[_fromClient].player.SetInput(_inputs, _rotation);
    }


}